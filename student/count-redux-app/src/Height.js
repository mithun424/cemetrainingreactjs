import React from "react";
import { connect } from "react-redux";

const Height = (props) => <div>Height is {props.height}</div>;

const mapStateToProps = (state) => {
  return {
    height: state.height,
  };
};
export default connect(mapStateToProps)(Height);
