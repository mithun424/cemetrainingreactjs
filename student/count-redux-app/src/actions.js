// Actions Creator functions that returns an action object

export const addCount = (count) => {
  return {
    type: "ADD_COUNT",
    payload: count,
  };
};

export const decrementCount = (count) => {
  return {
    type: "DECREMENT_COUNT",
  };
};
