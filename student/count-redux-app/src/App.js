import React from "react";
import { connect } from "react-redux";
import { addCount, decrementCount } from "./actions";
import Height from "./Height";

const App = (props) => (
  <React.Fragment>
    <div>App Component</div>
    <p>
      Count is {props.count}{" "}
      <button
        onClick={() => {
          props.addCount(props.count + 1);
        }}
      >
        Add Count
      </button>
      <button
        onClick={() => {
          props.decrementCount();
        }}
      >
        Decrement Count
      </button>
    </p>
    <p>Weight is {props.weight}</p>
    <Height />
  </React.Fragment>
);

// mapStateToProps receive the redux store/state
const mapStateToProps = (state) => {
  console.log("mapStateToProps Store is", state);
  return {
    count: state.count,
    weight: state.weight,
  };
};

const actionscreators = {
  addCount,
  decrementCount,
};

export default connect(mapStateToProps, actionscreators)(App);
