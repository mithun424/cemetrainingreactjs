import React from "react";
import ReactDOM from "react-dom";

import App from "./App";
import { Provider } from "react-redux";
import { createStore, combineReducers } from "redux";

const countReducer = (state = 0, action) => {
  //   if (state === undefined) {
  //     state = 1;
  //     return state;
  //   }
  console.log(`receive ${action.type} dispatch is countReducer`);

  if (action.type === "ADD_COUNT") {
    return action.payload;
  }
  if (action.type === "DECREMENT_COUNT") {
    return state - 1;
  }
  return state;
};

const weightReducer = (state = 100, action) => {
  console.log(`receive ${action.type} dispatch is weightReducer`);

  return state;
};

const heightReducer = (state = 185, action) => {
  console.log(`receive ${action.type} dispatch is heightReducer`);

  return state;
};

const reducers = combineReducers({
  count: countReducer,
  weight: weightReducer,
  height: heightReducer,
});

const store = createStore(reducers);

console.log("Store is: ", store.getState());

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById("root")
);
