const initialState = { entities: [] };

export default (state = initialState, action) => {
  console.log(`received ${action.type} dispatch in albumsReducer`);
  switch (action.type) {
    case "FETCH_ALBUMS_BEGIN":
    case "ADD_ALBUM_BEGIN":
      return { ...state, loading: true, error: null };
    case "FETCH_ALBUMS_SUCCESS":
      return { ...state, entities: action.payload, loading: false };
    case "FETCH_ALBUMS_FAILURE":
      return { ...state, entities: [], loading: false, error: action.payload };
    case "ADD_ALBUM_SUCCESS":
      return { ...state, loading: false };
    case "ADD_ALBUM_FAILURE":
      return { ...state, loading: false, error: action.payload };
    default:
      return state;
  }
};
