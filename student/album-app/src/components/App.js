import React, { useState, useEffect } from "react";
import { connect, useDispatch } from "react-redux";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import "./App.css";
import Navbar from "./Navbar";
import Banner from "./Banner";
import AlbumsListing from "./AlbumsListing";
import CreateAlbumForm from "./CreateAlbumForm";
import { fetchAlbums } from "../actions"; // pick up index.js by default

const App = () => {
  const dispatch = useDispatch();
  const [fetchAlbum, setFetchAlbum] = useState(false);

  useEffect(() => {
    dispatch(fetchAlbums()); // dispatch fetchAlbums action
  }, [fetchAlbum, fetchAlbums]); // run this at start and whenever fetchAlbum is changed

  return (
    <Router>
      <div className="app">
        <Navbar />
        <Switch>
          <Route exact path="/">
            <Banner />
            <AlbumsListing />
          </Route>
          <Route path="/add">
            <CreateAlbumForm
              fetchAlbum={fetchAlbum}
              setFetchAlbum={setFetchAlbum}
            />
          </Route>
        </Switch>
      </div>
    </Router>
  );
};

// const mapStatetoProps = (state) => {
// 	return {
// 		albums: state.albums, //we want something like props.albums
// 	};
// };

// const actionCreators = {
//   fetchAlbums,
// };

//export default connect(null, actionCreators)(App);
export default App;

// axios.get("http://localhost:8088/albums").then((res) => {
// 	console.log(res);
// 	setAlbums(res.data);
// });

// const albums = [
// {
// 	title: "Album 1",
// 	artist: "Artist 1",
// 	tracks: ["Track 1", "Track 2", "Track 3"],
// },
// 	{
// 		title: "Album 2",
// 		artist: "Artist 2",
// 		tracks: ["Track 4", "Track 5", "Track 6"],
// 	},
// 	{
// 		title: "Album 3",
// 		artist: "Artist 3",
// 		tracks: ["Track 6", "Track 7", "Track 9"],
// 	},
// 	{
// 		title: "Album 4",
// 		artist: "Artist 4",
// 		tracks: ["Track 1", "Track 2", "Track 3"],
// 	},
// 	{
// 		title: "Album 5",
// 		artist: "Artist 5",
// 		tracks: ["Track 4", "Track 5", "Track 6"],
// 	},
// 	{
// 		title: "Album 6",
// 		artist: "Artist 6",
// 		tracks: ["Track 6", "Track 7", "Track 9"],
// 	},
// ];
