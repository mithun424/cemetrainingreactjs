import React, { useState, useEffect } from "react";
import ReactDOM from "react-dom";

import Greet from "./Greet";
import Person from "./Person";

const PersonFunc = ({ name }) => {
  const [height, setHeight] = useState(185);
  const [weight, setWeight] = useState(200);

  useEffect(() => {
    console.log("It also renders");
  }, []);

  return (
    <div>
      <Greet person={name} name="Allstate" />
      <p>
        {name} weighs {weight}lbs and is {height}cm tall.
      </p>
      <button onClick={() => setWeight(weight + 1)}>Grow weight</button>
      <button onClick={() => setHeight(height + 1)}>Grow height</button>
    </div>
  );
};

const main = (
  <React.Fragment>
    <Person name="John" />
    <PersonFunc name="May" />
  </React.Fragment>
);
ReactDOM.render(
  //<Greet name="Allstate" person="John" />,
  main,
  document.querySelector("#root")
); // document.getElementById("root")
