import React from "react";

// Function component that receives properties as props
// function Greet(props) {
//   // {name: "Allstate"}
//   return <h1>Hello {props.name}!</h1>;
// }

// function Greet({ name }) {
//   // {name: "Allstate"}
//   return (<h1>Hello {name}!</h1>);
// }

// Arrow function
// const Greet = ({ name }) => <h1>Hello {name}!</h1>;

class Greet extends React.Component {
  //   constructor(props) {
  //     super(props);
  //   }
  render() {
    return (
      <h1>
        Hello {this.props.person} from {this.props.name}!!!
      </h1>
    );
  }
}

export default Greet;
