import React from "react";
import Greet from "./Greet";

class Person extends React.Component {
  constructor(props) {
    super(props);
    // set the initial state
    this.state = { height: 185, weight: 200 };

    // Without arrow function - this bind to function
    // this.grow = this.grow.bind(this);
  }

  growWeight = () => {
    // this.setState({ weight: this.state.weight + 1 });
    this.setState((state) => {
      return {
        weight: this.state.weight + 1,
      };
    });
  };

  growHeight = () => {
    // this.setState({ height: this.state.height + 1 });
    // this.setState( function( state ) { return { this.state.height + 1 } } );
    // this.setState((state) => ({ height: this.state.height + 1 }));
    this.setState((state) => {
      return {
        height: this.state.height + 1,
      };
    });
  };

  componentDidUpdate() {
    console.log("Its rendered!");
  }

  componentDidMount() {
    console.log("Its rendered!");
  }

  // Without arrow function - this bind to function
  //   grow() {
  //     this.setState({ height: 180 });
  //   }

  render() {
    return (
      <div>
        <Greet person={this.props.name} name="Allstate" />
        <p>
          {this.props.name} weighs {this.state.weight}lbs and is{" "}
          {this.state.height}cm tall.
        </p>
        <button onClick={this.growWeight}>Grow weight</button>
        <button onClick={this.growHeight}>Grow height</button>
      </div>
    );
  }
}

export default Person;
