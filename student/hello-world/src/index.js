import React from "react";
import ReactDOM from "react-dom";

import "./index.css";

// const header = React.createElement("h1", null, "My header");

// const text = React.createElement(
//   "p",
//   { className: "hello-text", id: "hello-id" },
//   "Hello World!!!"
// );

// const main = React.createElement("div", { className: "hello-container" }, [
//   header,
//   text,
// ]); // Parent

// const main = React.createElement("div", { id: "ice-cream" }, [
//   React.createElement("h1", null, "Ice Cream Flavours"),
//   React.createElement("ul", { class: "ice-cream-list" }, [
//     React.createElement("li", null, "Vanilla"),
//     React.createElement("li", null, "Chocolate"),
//     React.createElement("li", null, "Raspberry Ripple"),
//   ]),
// ]);

// this is doing using it JSX (javscript XML)
const shopname = "Allstate Ice Cream Shop";
const chocoStyle = { color: "brown" };
function getFlavours() {
  return (
    <React.Fragment>
      <h2>Ice Cream Flavours</h2>
      <ul className="ice-cream-list">
        <li>Vanilla</li>
        <li style={chocoStyle}>Chocolate</li>
        <li className="raseberry-color">Raspberry Ripple</li>
      </ul>
    </React.Fragment>
  );
}
const main = (
  <div id="root">
    <div>
      <h1> {shopname}</h1>
    </div>
    <div id="ice-cream">{getFlavours()}</div>
  </div>
);

ReactDOM.render(main, document.getElementById("root"));

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
// reportWebVitals();
