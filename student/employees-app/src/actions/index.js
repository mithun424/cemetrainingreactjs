import axios from "axios";

// for our Action Creators
export const fetchEmployeesBegin = () => {
  return {
    type: "FETCH_EMPLOYEES_BEGIN",
  };
};

export const fetchEmployeesSuccess = (albums) => {
  return {
    type: "FETCH_EMPLOYEES_SUCCESS",
    payload: albums,
  };
};

export const fetchEmployeesFailure = (err) => {
  return {
    type: "FETCH_EMPLOYEES_FAILURE",
    payload: { message: "Failed to fetch Employees.. please try again later" },
  };
};

// to be call by the components
export const fetchEmployees = () => {
  // returns the thunk function
  return (dispatch, getState) => {
    dispatch(fetchEmployeesBegin());
    console.log("state after fetchEmployeesBegin", getState());
    axios.get("http://localhost:8080/api/all").then(
      (res) => {
        //console.log(res);
        //setAlbums(res.data); // TODO dispatch FETCH_ALBUMS_SUCCESS
        setTimeout(() => {
          dispatch(fetchEmployeesSuccess(res.data));
          console.log("state after fetchEmployeesSuccess", getState());
        }, 3000);
      },
      (err) => {
        // dispatch FETCH_ALBUMS_FAILURE
        dispatch(fetchEmployeesFailure(err));
        console.log("state after fetchEmployeesFailure", getState());
      }
    );
  };
};

export const addEmployeeBegin = () => {
  return {
    type: "ADD_EMPOLYEE_BEGIN",
  };
};

export const addEmployeeSuccess = () => {
  return {
    type: "ADD_EMPOLYEE_SUCCESS",
  };
};

export const addEmployeeFailure = (err) => {
  return {
    type: "ADD_EMPOLYEE_FAILURE",
    payload: { message: "Failed to add new Employee.. please try again later" },
  };
};

export const addEmployee = (album) => {
  // returns our async thunk function
  return (dispatch, getState) => {
    dispatch(addEmployeeBegin());

    // return axios promise
    return axios.post("http://localhost:8080/api/save", album).then(
      () => {
        console.log("employee created!");
        // this is where we can dispatch ADD_ALBUM_SUCCESS
        dispatch(addEmployeeSuccess());
        return true;
      },
      (err) => {
        dispatch(addEmployeeFailure(err));
        console.log("state after addEmployeeFailure", getState());
        return false;
      }
    );
  };
};
