import React from "react";
import Employees from "./Employees";
import { useSelector } from "react-redux";

const EmployeeList = () => {
  const empolyees = useSelector((state) => state.empolyees.entities);
  const error = useSelector((state) => state.empolyees.error);
  const loading = useSelector((state) => state.empolyees.loading);

  if (error) {
    return <div className="d-flex justify-content-center">{error.message}</div>;
  }

  if (loading) {
    return (
      <div className="d-flex justify-content-center">
        <div
          className="spinner-border m-5"
          style={{ width: "4rem", height: "4rem" }}
          role="status"
        >
          <span className="sr-only">Loading...</span>
        </div>
      </div>
    );
  }

  return (
    <div className="album py-5">
      <div className="container">
        <div className="row">
          {empolyees.map((empolyee) => (
            <Employees
              key={empolyee.id}
              name={empolyee.name}
              age={empolyee.age}
              email={empolyee.email}
              salary={empolyee.salary}
            />
          ))}
        </div>
      </div>
    </div>
  );
};

export default EmployeeList;
