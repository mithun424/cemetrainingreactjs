import React from "react";

const EmployeeDetails = (props) => {
  return (
    props.visible && (
      <>
        <p>Employee Age: {props.age}</p>
        <p>Employee Email: {props.email}</p>
        <p>Employee Salary: {props.salary}</p>
      </>
    )
  );
};

export default EmployeeDetails;
