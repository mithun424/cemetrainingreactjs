import React, { useState } from "react";

import EmpolyeeName from "./EmpolyeeName";
import EmployeeDetails from "./EmployeeDetails";
import ShowHideButton from "./ShowHideButton";

const Employees = ({ name, age, email, salary }) => {
  const [visible, setVisible] = useState(true);

  return (
    <div className="card col-md-4" style={{ width: "18rem" }}>
      <div className="card-body">
        <EmpolyeeName name={name} />
        <ShowHideButton toggle={setVisible} visible={visible} />
        <EmployeeDetails
          age={age}
          email={email}
          salary={salary}
          visible={visible}
        />
      </div>
    </div>
  );
};

export default Employees;
