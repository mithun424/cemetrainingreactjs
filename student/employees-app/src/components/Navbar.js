import React from "react";
import { NavLink } from "react-router-dom";

const Navbar = () => (
  <nav className="nav nav-pills bg-light">
    <ul className="nav nav-pills">
      <li className="nav-item">
        <NavLink exact className="nav-link" to="/">
          Home
        </NavLink>
      </li>
      <li className="nav-item">
        <NavLink exact className="nav-link" to="/add">
          Add Employee
        </NavLink>
      </li>
    </ul>
  </nav>
);

export default Navbar;
