import React from "react";

const ShowHideButton = (props) => (
  <button
    className="btn btn-primary"
    onClick={() => props.toggle(!props.visible)}
  >
    {props.visible ? "Hide" : "Show More!"}
  </button>
);

export default ShowHideButton;
