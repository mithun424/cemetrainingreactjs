import React, { useState } from "react";
import axios from "axios";
import { useHistory } from "react-router-dom";
import { useDispatch } from "react-redux";
import { addEmployee } from "../actions";

const CreateEmployeeForm = ({ refresh, setRefresh }) => {
  const dispatch = useDispatch();
  const history = useHistory();
  const [name, setName] = useState("");
  const [age, setAge] = useState("");
  const [email, setEmail] = useState("");
  const [salary, setSalary] = useState("");

  const handleSubmit = async (e) => {
    e.preventDefault();

    const success = await dispatch(
      addEmployee({
        id: Math.floor(Math.random() * Math.floor(9999)),
        name: name,
        age: age,
        email: email,
        salary: salary,
      })
    );

    if (success) setRefresh(!refresh); // lifting state up to parent
    history.push("/");
  };
  return (
    <div className="container">
      <h3> Add New CreateEmployeeForm</h3>
      <form onSubmit={handleSubmit}>
        <div className="form-row">
          <div className="form-group col-md-5">
            <label>Name:</label>
            <input
              id="name"
              className="form-control"
              type="text"
              value={name}
              onChange={(e) => setName(e.target.value)}
            />
          </div>
          <div className="form-group col-md-5">
            <label>Age:</label>
            <input
              id="age"
              className="form-control"
              type="number"
              value={age}
              onChange={(e) => setAge(e.target.value)}
            />
          </div>
        </div>
        <div className="form-row">
          <div className="form-group col-md-5">
            <label>Email:</label>
            <input
              id="email"
              className="form-control"
              type="email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
          </div>
          <div className="form-group col-md-5">
            <label>Salary:</label>
            <input
              id="salary"
              className="form-control"
              type="number"
              value={salary}
              onChange={(e) => setSalary(e.target.value)}
            />
          </div>
        </div>
        <div>
          <input
            type="submit"
            className="btn btn-primary"
            value="Create Employee"
          />
        </div>
      </form>
    </div>
  );
};

export default CreateEmployeeForm;
