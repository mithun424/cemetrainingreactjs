import React, { useState, useEffect } from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import { useDispatch } from "react-redux";
import Banner from "./Banner";
import CreateEmployeeForm from "./CreateEmployeeForm";

import "./App.css";
import Navbar from "./Navbar";
import EmployeeList from "./EmployeeList";
import { fetchEmployees } from "../actions"; // pick up index.js by default

const App = () => {
  const dispatch = useDispatch();
  const [refresh, setRefresh] = useState(false);
  useEffect(() => {
    // axios.get("http://localhost:8080/api/all").then((resp) => {
    //   console.log(resp);
    //   setEmployees(resp.data);
    // });
    dispatch(fetchEmployees());
  }, [refresh, fetchEmployees]);

  return (
    <Router>
      <main role="main">
        <Navbar />
        <Switch>
          <Route path="/add">
            <CreateEmployeeForm refresh={refresh} setRefresh={setRefresh} />
          </Route>
          {/* <Route exact path="/">  */}
          <Route path="/">
            <Banner />
            {/* <div className="album py-5">
              <div className="container">
                <div className="row">
                  {empolyees.map((empolyee) => (
                    <Employees
                      key={empolyee.id}
                      name={empolyee.name}
                      age={empolyee.age}
                      email={empolyee.email}
                      salary={empolyee.salary}
                    />
                  ))}
                </div>
              </div>
            </div> */}
            <EmployeeList />
          </Route>
        </Switch>
      </main>
    </Router>
  );
};

export default App;

// const empolyees = [
//   {
//     id: 1,
//     name: "dee",
//     age: 22,
//     email: "d:i.ie",
//     salary: 2000,
//   },
//   {
//     id: 7,
//     name: "Mithun",
//     age: 22,
//     email: "d:i.ie",
//     salary: 2000,
//   },
//   {
//     id: 1232,
//     name: "Dee",
//     age: 50,
//     email: "d@i.ie",
//     salary: 1234,
//   },
// ];
