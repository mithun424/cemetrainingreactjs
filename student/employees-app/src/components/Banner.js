import React from "react";

const Banner = () => (
  <section className="jumbotron text-center">
    <div className="container">
      <h1 className="jumbotron-heading">Employee example</h1>
      <p className="lead text-muted">
        Something short and leading about the collection below—its contents, the
        creator, etc. Make it short and sweet, but not too short so folks don't
        simply skip over it entirely.
      </p>
      <p>
        <button className="btn btn-primary my-2">Main call to action</button>
      </p>
    </div>
  </section>
);

export default Banner;
