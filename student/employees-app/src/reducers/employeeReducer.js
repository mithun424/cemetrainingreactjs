const initialState = { entities: [] };

export default (state = initialState, action) => {
  console.log(`received ${action.type} dispatch in EmployeeReducer`);
  switch (action.type) {
    case "FETCH_EMPLOYEES_BEGIN":
    case "ADD_EMPLOYEE_BEGIN":
      return { ...state, loading: true, error: null };
    case "FETCH_EMPLOYEES_SUCCESS":
      return { ...state, entities: action.payload, loading: false };
    case "FETCH_EMPLOYEES_FAILURE":
      return { ...state, entities: [], loading: false, error: action.payload };
    case "ADD_EMPLOYEE_SUCCESS":
      return { ...state, loading: false };
    case "ADD_EMPLOYEE_FAILURE":
      return { ...state, loading: false, error: action.payload };
    default:
      return state;
  }
};
