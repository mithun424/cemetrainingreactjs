import { combineReducers } from "redux";
import employeeReducer from "./employeeReducer";

const rootReducer = combineReducers({
  empolyees: employeeReducer,
});

export default rootReducer;
